import pymysql
import boto3
import json

from lambda_decorators import cors_headers
from botocore.exceptions import ClientError

secret_name = "trellis.oribus.mementodev"
region_name = "us-west-2"

# Create a Secrets Manager client
session = boto3.session.Session()
client = session.client(
    service_name='secretsmanager',
    region_name=region_name
)

try:
    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )
except ClientError as e:
    if e.response['Error']['Code'] == 'DecryptionFailureException':
        # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
        # Deal with the exception here, and/or rethrow at your discretion.
        raise e
    elif e.response['Error']['Code'] == 'InternalServiceErrorException':
        # An error occurred on the server side.
        # Deal with the exception here, and/or rethrow at your discretion.
        raise e
    elif e.response['Error']['Code'] == 'InvalidParameterException':
        # You provided an invalid value for a parameter.
        # Deal with the exception here, and/or rethrow at your discretion.
        raise e
    elif e.response['Error']['Code'] == 'InvalidRequestException':
        # You provided a parameter value that is not valid for the current state of the resource.
        # Deal with the exception here, and/or rethrow at your discretion.
        raise e
    elif e.response['Error']['Code'] == 'ResourceNotFoundException':
        # We can't find the resource that you asked for.
        # Deal with the exception here, and/or rethrow at your discretion.
        raise e
else:
    # Decrypts secret using the associated KMS CMK.
    # Depending on whether the secret is a string or binary, one of these fields will be populated.
    if 'SecretString' in get_secret_value_response:
        secret = get_secret_value_response['SecretString']

secretDict = json.loads(secret)

connection = pymysql.connect(host='db-oribus-dev.chxxepniylv3.us-west-2.rds.amazonaws.com',
                             port=3306, 
                             user=secretDict['user'],
                             passwd=secretDict['pass'],
                             db='memento',
                             cursorclass=pymysql.cursors.DictCursor)

@cors_headers
def queue_counts(event, context):
    cursor = connection.cursor()
    cursor.execute("SELECT COUNT(*) FROM change_queue;")
    counts = {}
    counts['change_queue'] = cursor.fetchone()
    cursor.execute("SELECT COUNT(*) FROM job_queue;")
    counts['job_queue'] = cursor.fetchone()
    body = {
        "counts": counts
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    cursor.close()
    connection.commit()

    return response