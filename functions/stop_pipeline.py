import json

from lambda_decorators import cors_headers

from utils import OribusController

@cors_headers
def stop_pipeline(event, context):
    try:
        orb = OribusController()
        pipeline = event['path']['pipeline_id']
        orb.stop_pipeline(pipeline)

        response = {
            'statusCode': 200,
        }
        
        return response
    
    except Exception as e:
        return str(e)
