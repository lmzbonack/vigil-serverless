import json

from lambda_decorators import cors_headers

from utils import OribusController

@cors_headers
def start_oribus(event, context):
    try:
        ob = OribusController()
        ob.start_pipeline(ob.pipelines['errors'])
        ob.wait_for_status(ob.pipelines['errors'], 'RUNNING')

        ob.start_pipeline(ob.pipelines['enrollment-extract-soap'])
        ob.start_pipeline(ob.pipelines['population-extract-soap'])
        ob.start_pipeline(ob.pipelines['membership-extract-soap'])
        ob.reset_offsets(ob.pipelines['cdc'])
        ob.start_pipeline(ob.pipelines['cdc'])
        ob.start_pipeline(ob.pipelines['cleanup'])

        ob.start_pipeline(ob.pipelines['biodemo-extract'])
        ob.start_pipeline(ob.pipelines['enrollment-extract'])
        ob.start_pipeline(ob.pipelines['membership-extract'])
        ob.start_pipeline(ob.pipelines['population-extract'])

        ob.start_pipeline(ob.pipelines['plan-lookup-extract'])
        ob.start_pipeline(ob.pipelines['subplan-lookup-extract'])
        ob.start_pipeline(ob.pipelines['prog-lookup-extract'])
        ob.start_pipeline(ob.pipelines['student-group-lookup-extract'])
        ob.start_pipeline(ob.pipelines['term-lookup-extract'])

        ob.wait_for_status(ob.pipelines['enrollment-extract-soap'], 'RUNNING')
        ob.wait_for_status(ob.pipelines['membership-extract-soap'], 'RUNNING')
        ob.wait_for_status(ob.pipelines['population-extract-soap'], 'RUNNING')

        ob.start_pipeline(ob.pipelines['biodemo-extract-soap'])
        ob.wait_for_status(ob.pipelines['biodemo-extract-soap'], 'RUNNING')

        ob.start_pipeline(ob.pipelines['job-control'])
        ob.wait_for_status(ob.pipelines['job-control'], 'RUNNING')

        response = {
            'statusCode': 200,
        }
        return response
    
    except Exception as e:
        return str(e)
        