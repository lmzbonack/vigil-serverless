import json

from lambda_decorators import cors_headers

from utils import OribusController

@cors_headers
def stop_oribus(event, context):
    try:
        ob = OribusController()
        ob.stop_pipeline(ob.pipelines['job-control'])
        ob.wait_for_status(ob.pipelines['job-control'], 'STOPPED')

        ob.stop_pipeline(ob.pipelines['biodemo-extract-soap'])
        ob.wait_for_status(ob.pipelines['biodemo-extract-soap'], 'STOPPED')

        ob.stop_pipeline(ob.pipelines['enrollment-extract-soap'])
        ob.stop_pipeline(ob.pipelines['membership-extract-soap'])
        ob.stop_pipeline(ob.pipelines['population-extract-soap'])
        ob.stop_pipeline(ob.pipelines['cleanup'])
        ob.stop_pipeline(ob.pipelines['cdc'])

        ob.stop_pipeline(ob.pipelines['biodemo-extract'])
        ob.stop_pipeline(ob.pipelines['enrollment-extract'])
        ob.stop_pipeline(ob.pipelines['membership-extract'])
        ob.stop_pipeline(ob.pipelines['population-extract'])

        ob.stop_pipeline(ob.pipelines['plan-lookup-extract'])
        ob.stop_pipeline(ob.pipelines['subplan-lookup-extract'])
        ob.stop_pipeline(ob.pipelines['prog-lookup-extract'])
        ob.stop_pipeline(ob.pipelines['student-group-lookup-extract'])
        ob.stop_pipeline(ob.pipelines['term-lookup-extract'])

        ob.wait_for_status(ob.pipelines['enrollment-extract-soap'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['membership-extract-soap'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['population-extract-soap'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['cleanup'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['cdc'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['biodemo-extract'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['enrollment-extract'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['membership-extract'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['population-extract'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['plan-lookup-extract'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['subplan-lookup-extract'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['prog-lookup-extract'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['student-group-lookup-extract'], 'STOPPED')
        ob.wait_for_status(ob.pipelines['term-lookup-extract'], 'STOPPED')

        ob.stop_pipeline(ob.pipelines['errors'])
        ob.wait_for_status(ob.pipelines['errors'], 'STOPPED')

        response = {
            'statusCode': 200,
        }
        return response
        
    except Exception as e:
        return str(e)