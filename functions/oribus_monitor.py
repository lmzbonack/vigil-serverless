import requests
import os
import json

from utils import slackwebhook, Attachment

slack = slackwebhook('https://hooks.slack.com/services/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')

# If you care about it running put it in this list
trackedPipelineNames = [
    'oribuspscdcv03',
    'oribuserrorsv01',
    'lmzbonackdevoribuschangesjobsv03',
    'sadevsfdevextractsbiodemoStudentSOAP',
    'sadevsfdevextractsenrollmentSOAP',
    'sadevsfdevextractspopulationSOAP',
    'devoribusjobsextractscleanup',
    'sadevsfdevextractsbiodemoStudent',
    'sadevsfdevextractsenrollment',
    'sadevsfdevextractsstudentGroupMembership',
    'sadevsfdevextractsstudentGroupMembershipSOAP',
    'sadevsfdevextractspopulation',
    'sadevsfdevextractsplanLookup',
    'sadevsfdevextractssubplanLookup',
    'sadevsfdevextractsprogLookup',
    'sadevsfdevextractsstudentGroupLookup',
    'sadevsfdevextractstermLookup'
]

def oribus_monitor(event, context):
    try:
        req = requests.get('http://ssdc-ports.test.trellis.arizona.edu:18660/rest/v1/pipelines/status',
            auth = (os.getenv("SSDC_USERNAME"),os.getenv("SSDC_PASSWORD"))
            )
        if req.status_code == 200:
            broken_pipelines = []
            res = json.loads(req.content)
            for item in res:
                if item[:-36] in trackedPipelineNames:
                    if res[item]['status'] != 'RUNNING':
                        broken_pipelines.append(item[:-36])
            
            if len(broken_pipelines) > 1:
                message = Attachment(pretext="The following pipelines got hit by a bus",
                            fallback="Honk Honk",
                            color="#e81a1a",
                            text="We got hit by a bus: {}".format(', '.join(broken_pipelines))
                        )
                messages = []
                messages.append(message)
                slack.post(messages)
             
        else:
            message = Attachment(pretext="Oribus was unable to return pipeline status data",
                        fallback="Failed with status code {}".format(req.status_code),
                        color="#e81a1a",
                        text=res)
            messages = []
            messages.append(message)
            slack.post(messages)
        
    except Exception as e:
        return str(e)
        message = Attachment(pretext="Oribus's monitoring lambda encountered an unexpected error",
            fallback=e,
            color="#e81a1a",
            text=e)
        messages = []
        messages.append(message)
        slack.post(messages)
