import requests
import os
import json
import time

class OribusController:
    """
    Class to enable easy startup and shutdown of Oribus
    """

    def __init__(self):
        self.pipelines = {
            "cdc": "oribuspscdcv0345200e80-5eb7-48b4-a5a7-c8540a87f2be",
            "errors": "oribuserrorsv01bc229f62-8673-4c66-8057-0da3c44cae2e",
            "cleanup": "devoribusjobsextractscleanup20459e0f-2347-4bbc-bd43-332f902a927e",
            "job-control": "lmzbonackdevoribuschangesjobsv0308a4576f-5ec6-4ea0-a8a0-8e539020f4e7",
            "biodemo-extract-soap": "sadevsfdevextractsbiodemoStudentSOAPcf704786-2fd4-499e-8d67-6d8d25e2d14b",
            "enrollment-extract-soap": "sadevsfdevextractsenrollmentSOAP6d2d64c1-f4bb-4128-9e81-dd93b7aab908",
            "membership-extract-soap": "sadevsfdevextractsstudentGroupMembershipSOAPcfc1d945-cd63-4c98-97fd-a564fe511a19",
            "population-extract-soap": "sadevsfdevextractspopulationSOAPfda08258-6cf3-4ae4-a258-c504021c3e48",
            "biodemo-extract": "sadevsfdevextractsbiodemoStudent79f0a91c-1581-4d2d-8c20-0802fd67b954",
            "enrollment-extract": "sadevsfdevextractsenrollmentbc364349-7193-4517-a164-271f0eda4dcc",
            "membership-extract": "sadevsfdevextractsstudentGroupMembership42cd8c55-f9e2-4c32-b105-7a8bf98d3a2d",
            "population-extract": "sadevsfdevextractspopulationde42e974-c343-4415-9c1e-7d9db521e56f",
            "plan-lookup-extract": "sadevsfdevextractsplanLookupdce6bbd5-1f44-480c-be8c-290df40dba83",
            "subplan-lookup-extract": "sadevsfdevextractssubplanLookupc5c9cd3e-585e-40c9-b686-32e5b51e8e5f",
            "prog-lookup-extract": "sadevsfdevextractsprogLookup5d1b6206-53ff-4ee7-ade8-60a970b7d0fe",
            "student-group-lookup-extract": "sadevsfdevextractsstudentGroupLookup549bb1f0-4e4f-4175-b560-d97f832413c7",
            "term-lookup-extract": "sadevsfdevextractstermLookupcb832dba-7e26-4539-a388-3f9f3bbf4dee"
        }

    def get_pipeline_status(self, pipeline):
        req = requests.get('{}/rest/v1/pipeline/{}/status'.format(os.getenv("SSDC_SERVER"),pipeline),
                           auth = (os.getenv("SSDC_USERNAME"),os.getenv("SSDC_PASSWORD"))
                          )
        res = json.loads(req.content)
        return(res['status'])

    def start_pipeline(self, pipeline):
        req = requests.post('{}/rest/v1/pipeline/{}/start'.format(os.getenv("SSDC_SERVER"),pipeline),
                            auth = (os.getenv("SSDC_USERNAME"),os.getenv("SSDC_PASSWORD")),
                            headers={'X-Requested-By': 'sdc_time'},
                            data={}
                          )
        if req.status_code == 200:
            print('{} started'.format(pipeline))
        else:
            print('{} failed to start with error code {}'.format(pipeline, req.status_code))

    def stop_pipeline(self, pipeline):
        req = requests.post('{}/rest/v1/pipeline/{}/stop'.format(os.getenv("SSDC_SERVER"),pipeline),
                            auth = (os.getenv("SSDC_USERNAME"),os.getenv("SSDC_PASSWORD")),
                            headers={'X-Requested-By': 'sdc_time'},
                            data={}
                          )
        if req.status_code == 200:
            print('{} stopped'.format(pipeline))
        else:
            print('{} failed to stop with error code {}'.format(pipeline, req.status_code))

    def reset_offsets(self, pipeline):
        req = requests.post('{}/rest/v1/pipeline/{}/resetOffset'.format(os.getenv("SSDC_SERVER"),pipeline),
                            auth = (os.getenv("SSDC_USERNAME"),os.getenv("SSDC_PASSWORD")),
                            headers={'X-Requested-By': 'sdc_time'},
                            data={}
                          )
        if req.status_code == 200:
            print('{} stopped'.format(pipeline))
        else:
            print('{} failed to stop with error code {}'.format(pipeline, req.status_code))  

    def wait_for_status(self, pipeline, status):
        while(True):
            if self.get_pipeline_status(pipeline) == status: break
            time.sleep(2)
        return

################################################################################
# Forked from https://github.com/carlcortright/slack-webhook and customized for
# this app's use of slack webhooks.
#
# Easy python interface for posting updates to a slack channel.
#
# Original Author: Carl Cortright
# Date: 12/20/2016
#
################################################################################
class slackwebhook:
    """Slack Webhook interaction."""

    slack_box_delivery = None

    def __init__(self, webhook_url):
        self.webhook_url = webhook_url

    def post(self, attachments):
        """ Post to the slack webhook with attachment support."""
        slackwebhook.slack_bot_delivery = attachments
        payload = "{ \"attachments\": %s }" % (str([a for a in attachments]))
        status = self.__post_payload(str(payload))
        print(status)
        return status

    def __post_payload(self, payload):
        """Post a json payload to slack webhook url."""
        response = requests.post(self.webhook_url, data=payload)
        if response.status_code != 200:
            print("ERROR: the url %s returned a %d response code."
                  % (self.webhook_url, response.status_code))
            print(response.content)
        return response.status_code

class Attachment:
    """An object to represent a slack message attachment."""

    def __init__(self,
                 fallback=None,
                 color=None,
                 pretext=None,
                 title=None,
                 text=None):
        self.fallback = fallback
        self.color = color
        self.pretext = pretext
        self.title = title
        self.text = text

    def __repr__(self):
        """Serialize the object to json."""
        return json.dumps(self.__dict__)